# Build ICON with python support

```term
./build.sh
```

does the following:
- Sets up an virtual python environment (in the subdirectory `venv`, activate with `source venv/bin/activate`)
  - installs all usefull python packages in this venv
- downloads icon
- configures ICON properly
- builds ICON
- creates a runscript that runs a coupled experiment with hiopy output

This experiment can be run by calling `./exp.esm_bb_ruby0_hiopy.run`
in an interactive session on levante, or by starting a new job with
`sbatch exp.esm_bb_ruby0_hiopy.run`. Both ways need to be started from the `icon/build/run` directory.
