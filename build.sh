#!/usr/bin/bash

set -e
#set -x # print commands before executing, uncomment to debug

export MAKEFLAGS="-j32"

if [[ "$1" == "clean" ]]; then
    rm -rf icon venv outdata
fi

# clean env
module purge
spack unload --all

#module load python3 # <- Does not work yet
module load openmpi/4.1.2-gcc-11.2.0
module load netcdf-c/4.8.1-gcc-11.2.0

if [ ! -d "venv" ]; then
    /sw/spack-levante/python-3.9.9-fwvsvi/bin/python -m venv venv --prompt icon
    venv/bin/pip install --upgrade pip
fi
source venv/bin/activate
pip install -r requirements.txt

if [ ! -d "icon" ]; then
    #git clone --recursive git@gitlab.dkrz.de:icon/icon.git
    curl https://gitlab.dkrz.de/icon/icon-model/-/archive/release-2024.07-public/icon-model-release-2024.07-public.tar.gz | tar -xzf -
    mv icon-model-release-2024.07-public icon
fi

cd icon

mkdir -p build
cd build
../config/dkrz/levante.intel \
    --enable-python-bindings --with-pic \
    --enable-openmp \
    --disable-mpi-checks \
    --enable-comin

make | tee make.log

# install the yac python package into the venv
(cd externals/yac/python && pip install .)

# build the comin python adapter
(cd externals/comin/build && cmake -DCOMIN_ENABLE_PYTHON_ADAPTER=ON . && make)

if [ ! -f run/exp.esm_bb_ruby0_hiopy ]; then
    cp run/exp.esm_bb_ruby0 run/exp.esm_bb_ruby0_hiopy
    cat ../../hiopy_run_script_extension >> run/exp.esm_bb_ruby0_hiopy
fi
./make_runscripts esm_bb_ruby0_hiopy

(cd run && ln -s ../../../exp.slo1684.run)
