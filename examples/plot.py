#!/usr/bin/env python3

# Example script to demonstrate ICON coupling to python3
#
# To use it:
# - make sure `coupled_to_output` is enabled. In your atm or oce namelist:
#  !
#  &coupling_mode_nml
#    coupled_to_output          = .TRUE.
#  /
#
# - to run this script in the MPI environment add a mpmd file to the runscript and adapt START_MODEL:
# spack load /fwv # python
#
# cat > mpmd.conf <<EOF
# 0 python3 PATH_TO_THIS_FILE
# * ${MODEL}
# EOF
#
# START_MODEL="${START_MODEL:=$START --multi-prog mpmd.conf}"
#

import os
import yac as yac_module
import numpy as np
import matplotlib.pyplot as plt
import logging

logging.basicConfig(level=logging.INFO)

var = ("atm_output", "icon_atmos_grid", "tas")

yac_module.def_calendar(yac_module.Calendar.PROLEPTIC_GREGORIAN)
yac = yac_module.YAC(default_instance=True)

comp = yac.def_comp("plot")
yac.sync_def()

x = np.linspace(-np.pi, np.pi, 360)
y = np.linspace(-0.5*np.pi, 0.5*np.pi, 180)
grid = yac_module.Reg2dGrid("plot_grid", x, y)
points = grid.def_points(yac_module.Location.CORNER, x, y)

nnn = yac_module.InterpolationStack()
nnn.add_nnn(yac_module.NNNReductionType.AVG, 1, 1.)

logging.info(yac.component_names)
timestep = yac.get_field_timestep(*var)
collection_size = yac.get_field_collection_size(*var)
field = yac_module.Field.create(var[2], comp, points, 1,
                                timestep, yac_module.TimeUnit.ISO_FORMAT)

yac.def_couple(*var,
               "plot", "plot_grid", var[2],
               timestep, yac_module.TimeUnit.ISO_FORMAT, 0, nnn)

for i in range(field.collection_size):
    os.makedirs(f"{field.name}_{i}", exist_ok=True)

yac.enddef()

info = yac_module.Action.COUPLING
while info != yac_module.Action.GET_FOR_RESTART:
    time = field.datetime
    print(f"plotting {field.name} at {time}")
    buf, info = field.get()
    for i in range(buf.shape[0]):
        plt.imshow(buf[i, :].reshape((len(y), len(x)))[::-1, :])
        plt.title(f"{field.name} - {time}")
        plt.colorbar()
        plt.savefig(f"{field.name}_{i}/{time}.png")
        plt.clf()
